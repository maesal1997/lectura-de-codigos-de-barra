import { Injectable } from '@angular/core';
import Swal, { SweetAlertIcon } from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class AlertasConfirmacion {
  respuesta: boolean;
  // tslint:disable-next-line: typedef
  async mensaje(
    titulo: string,
    mensaje: string,
    tipo: SweetAlertIcon
  ) {
   await Swal.fire({
      title: titulo,
      text: mensaje,
      icon: tipo,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      showClass: {
        popup: 'animate__animated animate__fadeInDown',
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp',
      },
    }).then((result) => {
      if (result.value) {
        this.respuesta = true;
      }
      else {
        this.respuesta = false;
      }
    });
    return this.respuesta;
  }

  // tslint:disable-next-line: typedef
  informacion(titulo: string, mensaje: string, tipo: SweetAlertIcon) {
    Swal.fire({
      title: titulo,
      text: mensaje,
      icon: tipo,
      confirmButtonColor: '#3085d6',
      confirmButtonText: 'Aceptar',
      showClass: {
        popup: 'animate__animated animate__fadeInDown',
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp',
      },
    });
  }
}
