
import { Injectable } from '@angular/core';
import { BotonesGridComponent } from '../boton-grid/botonGridServicios.component';
@Injectable({
  providedIn: 'root'
})


export class ColumnUnidades {

  // tslint:disable-next-line: typedef
  columnasGridUnidades() {
   // tslint:disable-next-line: prefer-const
   let columnDefs = [
    {
      headerName: 'Código Unidad',
      sortable: true,
      filter: true,
      valueGetter: 'data.codigoUnidad',
    },
    {
      headerName: 'Tipo Unidad',
      sortable: true,
      filter: true,
      valueGetter: 'data.tipoUnidad',
    },
    {
      headerName: 'Peso Real KG',
      sortable: true,
      filter: true,
      valueGetter: 'data.pesoReal',
    },
    {
      headerName: 'Largo CM',
      sortable: true,
      filter: true,
      valueGetter: 'data.largo',
    },
    {
      headerName: 'Alto CM',
      sortable: true,
      filter: true,
      valueGetter: 'data.alto',
    },
    {
      headerName: 'Ancho CM',
      sortable: true,
      filter: true,
      valueGetter: 'data.ancho',
    },
    {
      headerName: 'Nuevo Código',
      field: 'nuevoCodigo',
      sortable: true,
      filter: true,
      editable: true
    },
    {
      width: 50,
      cellRendererFramework:  BotonesGridComponent
    },

  ];
   return columnDefs;
  }
}
