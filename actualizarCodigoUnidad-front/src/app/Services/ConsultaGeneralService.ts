import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { timeout, catchError } from 'rxjs/operators';
import { of } from 'rxjs/internal/observable/of';

@Injectable({
    providedIn: 'root'
})

  export class ConsultaGeneralService {
    ENDPOINT = '';
    constructor(private httpClient: HttpClient) {
        this.ENDPOINT =  'http://localhost:7001/codigo-unidades/api/consulta';
    }
    // tslint:disable-next-line: typedef
    consultaGeneralPorFiltros(numeroGuia: any) {
        // tslint:disable-next-line: max-line-length
        return this.httpClient.post<any[]>(`${this.ENDPOINT}/general?numeroGuia=${numeroGuia}`,  {withCredentials: true });
    }
  }
