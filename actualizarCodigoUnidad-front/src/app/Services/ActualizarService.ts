import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})

export class ActualizarService {
  ENDPOINT = '';
  constructor(private httpClient: HttpClient) {
    this.ENDPOINT = 'http://localhost:7001/codigo-unidades/api';
  }
  // tslint:disable-next-line: typedef
  consultarDatosUsuario(){
    return this.httpClient.get<any[]>(`${this.ENDPOINT}/consulta/usuario`,  {withCredentials: true });
  }
  // tslint:disable-next-line: typedef
  actualizarCodigoUnidad(data: any, usuario: any, ciudad: any) {
  /*const headers = new HttpHeaders({
  'Content-Type': 'application/json', AccessToken: token });
  const AccessToken = { headers };*/
    // tslint:disable-next-line: max-line-length
  return this.httpClient.post<any[]>(`${this.ENDPOINT}/actualizar/codigo/unidad?usuario=${usuario}&ciudad=${ciudad}`, data,  {withCredentials: true });
  }

}
