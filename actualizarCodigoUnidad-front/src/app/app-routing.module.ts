import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VistaPrincipalComponent } from './components/vista-principal/vista-principal.component';

const routes: Routes = [
  { path: 'actualizarCodigoUnidad', component: VistaPrincipalComponent},
  { path: '', component: VistaPrincipalComponent, pathMatch: 'full'},
  { path: '**', pathMatch: 'full', redirectTo: 'actualizarCodigoUnidad' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
