package co.com.tcc.maestro.config;

import java.util.Locale;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@ComponentScan(basePackages = { "co.com.tcc.maestro.*" })
@PropertySources({
		// @PropertySource("classpath:sql.properties"),
})
@EnableWebMvc
@EnableTransactionManagement
public class AppConfig extends WebMvcConfigurerAdapter {

	@Bean(name = "multipartResolver")
	public MultipartResolver multipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		// multipartResolver.setMaxUploadSize(104857600); // 10MB
		multipartResolver.setMaxUploadSizePerFile(-1); // 1MB

		return multipartResolver;
	}

	@Bean
	public ViewResolver getViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		return resolver;
	}

	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();

		messageSource.setBasename("classpath:i18n/messages");
		messageSource.setUseCodeAsDefaultMessage(true);
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

	@Bean
	public LocaleResolver localeResolver() {
		CookieLocaleResolver resolver = new CookieLocaleResolver();
		resolver.setDefaultLocale(new Locale("es"));
		resolver.setCookieName("myLocaleCookie");

		return resolver;
	}

	public void addInterceptors(InterceptorRegistry registry) {
		LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
		interceptor.setParamName("mylocale");
		registry.addInterceptor(interceptor);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
		registry.addResourceHandler("/components/**").addResourceLocations("/components/");
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	@Bean
	public JdbcTemplate jdbcTemplate() throws NamingException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(getDataSource());
		return jdbcTemplate;
	}

	@Bean
	@Qualifier("jdbcTemplateConsultaGeneral")
	public JdbcTemplate getJdbcTemplateMovilidadDMR() throws NamingException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(getDataSourceMovilidadDMR());
		return jdbcTemplate;
	}

	@Bean
	@Qualifier("jdbcTemplateConsultaGeneral")
	public NamedParameterJdbcTemplate getJdbcTemplateNamedParameter() throws NamingException {
      
		return new NamedParameterJdbcTemplate(getDataSourceMovilidadDMR());

	}

	@Bean
	public DataSource getDataSourceMovilidadDMR() throws NamingException {
		JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
		String jndiName = "jdbc/TCC_MovilidadDMR";
		bean.setJndiName(jndiName);
		bean.setProxyInterface(DataSource.class);
		bean.setLookupOnStartup(false);
		bean.afterPropertiesSet();
		return (DataSource) bean.getObject();
	}

	@Bean
	public DataSource getDataSource() throws NamingException {
		JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
		String jndiName = "jdbc/Staging";
		bean.setJndiName(jndiName);
		bean.setProxyInterface(DataSource.class);
		bean.setLookupOnStartup(false);
		bean.afterPropertiesSet();
		return (DataSource) bean.getObject();
	}

	@Bean
	public DataSourceTransactionManager getDataSourceTransactionManager() throws NamingException {
		DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
		dataSourceTransactionManager.setDataSource(getDataSource());
		return dataSourceTransactionManager;
	}
//	@Bean
//	public ObjectMapper objectMapper() {
//
//	    DateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy hh:mm:ss a");
//	    dateFormat.setTimeZone(TimeZone.getTimeZone("America/Bogota"));
//
//	    ObjectMapper mapper = new ObjectMapper();
//	    mapper.setDateFormat(dateFormat);
//	    return mapper;
//	}

}