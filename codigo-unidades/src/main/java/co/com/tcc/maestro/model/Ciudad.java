package co.com.tcc.maestro.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

public class Ciudad {

	@JsonSerialize(using = ToStringSerializer.class)
	private BigDecimal id;

	private String descripcion;

	private String abreviatura;

	public Ciudad() {
		// TODO Auto-generated constructor stub
	}

	public Ciudad(BigDecimal id, String descripcion, String abreviatura) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.abreviatura = abreviatura;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}

	@Override
	public String toString() {
		return "Ciudad [id=" + id + ", descripcion=" + descripcion + ", abreviatura=" + abreviatura + "]";
	}

}
