package co.com.tcc.maestro.web.api;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.tcc.maestro.utils.UsuarioLogueado;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UsuarioRestController {

	private static final String API = "api/usuario/";

	@GetMapping(value = API + "roles")
	public List<Object> listarRoles() {

		return UsuarioLogueado.listarRoles();
	}

}
