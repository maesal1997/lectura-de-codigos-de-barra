package co.com.tcc.maestro.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tcc.maestro.dao.TokenDao;
import co.com.tcc.maestro.dto.ObjMensajes;
import co.com.tcc.maestro.exception.ValidacionDominioException;
import co.com.tcc.maestro.service.TokenService;

@Service
public class TokenServiceImpl implements TokenService {

	@Autowired
	private TokenDao tokenDao;

	@Override
	public void validarToken(String token) {
		
		if (tokenDao.validarToken(token).isEmpty()) {
			ObjMensajes objMensajes = new ObjMensajes();
			objMensajes.setMensajeSistema("Token inv�lido");
		    objMensajes.setMensajeUsuario("Token inv�lido");		
			throw new ValidacionDominioException(objMensajes);
		}

	}

}
