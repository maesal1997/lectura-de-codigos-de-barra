package co.com.tcc.maestro.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import co.com.tcc.maestro.dao.TokenDao;

@Repository
public class TokenDaoImpl implements TokenDao {

	@Autowired
	@Qualifier("jdbcTemplateConsultaGeneral")
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<String> validarToken(String token) {

		String sql = " SELECT TOKE_LISTA_ROLES FROM INS_TOKEN_USUARIO WHERE TOKE_TOKEN_VALUE = ?";
		return jdbcTemplate.queryForList(sql, new Object[] { token }, String.class);
	}

}
