package co.com.tcc.maestro.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import co.com.tcc.maestro.dao.rowmapper.ParametroRowMapper;
import co.com.tcc.maestro.exception.AccesoDatosException;
import co.com.tcc.maestro.model.Parametro;
import co.com.tcc.maestro.service.Messages;

/**
 *
 * @author Joan Roa
 */

@Repository
public class ParametrosDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private Messages messages;

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public String consultarParametro(String parametro) {

		String sql = "SELECT PARA_VALOR FROM MAE_PARAMETROS_SISTEMA WHERE PARA_NOMBRE = ?";

		List<String> result = jdbcTemplate.queryForList(sql, new Object[] { parametro }, String.class);

		if (result.isEmpty()) {

			throw new AccesoDatosException(
					messages.get("parametros.dao.noresultados").replace("#PARAMETRO", parametro));
		}

		return result.get(0);

	}

	/**
	 * 
	 * @param parametro : Nombre del parámetro a consultar en MAE_PARAMETROS_SISTEMA
	 * @param clase     (Class): El tipo de dato que desea retornar
	 * @return
	 */
	public Object consultarParametro(String parametro, Class<?> clase) {

		String sql = "SELECT PARA_VALOR FROM MAE_PARAMETROS_SISTEMA WHERE PARA_NOMBRE = ?";

		List<?> results = jdbcTemplate.queryForList(sql, new Object[] { parametro }, clase);

		if (results.isEmpty()) {

			throw new AccesoDatosException(
					messages.get("parametros.dao.noresultados").replace("#PARAMETRO", parametro));

		}

		return results.get(0);

	}

	public Map<String, Object> consultarParametros(List<String> nombresParametros) {

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("parametros", nombresParametros);

		String sql = "SELECT * FROM MAE_PARAMETROS_SISTEMA WHERE PARA_NOMBRE IN (:parametros)";

		List<Parametro> results = namedParameterJdbcTemplate.query(sql, parameters, new ParametroRowMapper());

		if (nombresParametros.size() > results.size())
			throw new AccesoDatosException(messages.get("parametros.dao.params.dinamicos"));

		if (nombresParametros.size() < results.size())
			throw new AccesoDatosException(messages.get("parametros.dao.params.sobrantes"));

		Map<String, Object> retorno = new HashMap<String, Object>();
		for (Parametro parametro : results) {

			retorno.put(parametro.getNombre(), parametro.getValor());

		}

		return retorno;

	}

}
