package co.com.tcc.maestro.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import co.com.tcc.maestro.dao.IDatosConsumirActuApp;
import co.com.tcc.maestro.dao.rowmapper.DatosUserAppRowMapper;
import co.com.tcc.maestro.dao.rowmapper.DatosUserTokenRowMapper;
import co.com.tcc.maestro.dto.DatosUser;
import co.com.tcc.maestro.utils.UsuarioLogueado;

@Repository
public class DatosConsumirActuAppDaoImpl implements IDatosConsumirActuApp{

	@Autowired
	@Qualifier("jdbcTemplateConsultaGeneral")
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<DatosUser> obtenerUsuario() {
	
		List<DatosUser> datosUsuarioApp = new ArrayList<DatosUser>();
		String sql = "SELECT USUA_LOGIN, CIUD_ID_INT FROM SEG_USUARIOS WHERE USUA_LOGIN = ?";
		 datosUsuarioApp =  jdbcTemplate.query(sql, new Object[] { UsuarioLogueado.getUsuario() }, new DatosUserAppRowMapper());
		return datosUsuarioApp;
	}
	
	@Override
	public List<DatosUser> obtenerCiudadUsuarioToken(String token) {
		List<DatosUser> datosUsuarioApp = new ArrayList<DatosUser>();
		String sql = "SELECT DIRE.CIUD_ID_INT, TOKE.TOKE_USUARIO                                     \r\n" + 
				"        FROM MAE_LOCALIZACION LOCA                                                  \r\n" + 
				"        INNER JOIN MAE_DIRECCIONES DIRE ON LOCA.DIRE_ID_INT = DIRE.DIRE_ID_INT      \r\n" + 
				"        INNER JOIN INS_TOKEN_USUARIO TOKE ON LOCA.LOCA_ID_INT = TOKE.TOKE_REGIONAL  \r\n" + 
				"        WHERE TOKE_TOKEN_VALUE = '"+token+"'";
		
		 datosUsuarioApp =  jdbcTemplate.query(sql, new DatosUserTokenRowMapper());
		 
			return datosUsuarioApp;
	}

}
