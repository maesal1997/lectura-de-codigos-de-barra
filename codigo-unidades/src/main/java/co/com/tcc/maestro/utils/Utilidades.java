package co.com.tcc.maestro.utils;

public class Utilidades {

	public static String nullToEmpty(String s) {
		if (s == null || s.equals("null") || s.equals("NULL")) {
			return "";
		}
		return s;
	}

}
