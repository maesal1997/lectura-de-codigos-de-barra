package co.com.tcc.maestro.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tcc.maestro.dao.IValidarRemesaDao;
import co.com.tcc.maestro.dto.ObjMensajes;
import co.com.tcc.maestro.exception.ValidacionDominioException;
import co.com.tcc.maestro.service.IValidarRemesaService;

@Service
public class ValidarRemesaServiceImpl implements IValidarRemesaService{

	
	@Autowired
	private IValidarRemesaDao validarRemesaDao;
	@Override
	public void validarRemesa(String numeroGuia) {
		if (validarRemesaDao.validarRemesa(numeroGuia).isEmpty()) {
			ObjMensajes objMensajes = new ObjMensajes();
			objMensajes.setMensajeSistema("El n�mero de gu�a digitado no existe");
		    objMensajes.setMensajeUsuario("El n�mero de gu�a digitado no existe");		
			throw new ValidacionDominioException(objMensajes);
		}
		
	}

}
