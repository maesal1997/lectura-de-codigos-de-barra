package co.com.tcc.maestro.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

public class Tercero {

	@JsonSerialize(using = ToStringSerializer.class)
	private BigDecimal id;

	private String identificacion;

	private TipoIdentificacion tipoIdentificacion;

	private String usuario;

	private InformacionPago informacionPago;

	private String telefonoCelular;

	private String email;

	private String direccionResidencia;

	private boolean empleadoTCC;

	public Tercero() {
	}

	public Tercero(BigDecimal id, String identificacion) {
		super();
		this.id = id;
		this.identificacion = identificacion;
	}

	public boolean isEmpleadoTCC() {
		return empleadoTCC;
	}

	public void setEmpleadoTCC(boolean empleadoTCC) {
		this.empleadoTCC = empleadoTCC;
	}

	public TipoIdentificacion getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(TipoIdentificacion tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public InformacionPago getInformacionPago() {
		return informacionPago;
	}

	public void setInformacionPago(InformacionPago informacionPago) {
		this.informacionPago = informacionPago;
	}

	public String getTelefonoCelular() {
		return telefonoCelular;
	}

	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDireccionResidencia() {
		return direccionResidencia;
	}

	public void setDireccionResidencia(String direccionResidencia) {
		this.direccionResidencia = direccionResidencia;
	}

}
