package co.com.tcc.maestro.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

public class ResultadoConsultaGeneral {
	
	private String nombreRemite;
	private String nitRemite;
	private String ciudadRemite;
	private String direccionRemite;
	private String nombreDestinatario;
	private String nitDestinatario;
	private String ciudadDestinatario;
	private String direccionDestinatario;
	private String unidadNegocio;
	private String estadoOperativo;
	private String codigoUnidad;
	private String tipoUnidad;
	private String pesoReal;
	private String largo;
	private String alto;
	private String ancho;
	@JsonSerialize(using = ToStringSerializer.class)
	private BigDecimal idUnidad;
	private String nuevoCodigo;
	private String ubicacionTcc;
	
	public String getNombreRemite() {
		return nombreRemite;
	}
	public void setNombreRemite(String nombreRemite) {
		this.nombreRemite = nombreRemite;
	}
	public String getNitRemite() {
		return nitRemite;
	}
	public void setNitRemite(String nitRemite) {
		this.nitRemite = nitRemite;
	}
	public String getCiudadRemite() {
		return ciudadRemite;
	}
	public void setCiudadRemite(String ciudadRemite) {
		this.ciudadRemite = ciudadRemite;
	}
	public String getDireccionRemite() {
		return direccionRemite;
	}
	public void setDireccionRemite(String direccionRemite) {
		this.direccionRemite = direccionRemite;
	}
	public String getNombreDestinatario() {
		return nombreDestinatario;
	}
	public void setNombreDestinatario(String nombreDestinatario) {
		this.nombreDestinatario = nombreDestinatario;
	}
	public String getNitDestinatario() {
		return nitDestinatario;
	}
	public void setNitDestinatario(String nitDestinatario) {
		this.nitDestinatario = nitDestinatario;
	}
	public String getCiudadDestinatario() {
		return ciudadDestinatario;
	}
	public void setCiudadDestinatario(String ciudadDestinatario) {
		this.ciudadDestinatario = ciudadDestinatario;
	}
	public String getDireccionDestinatario() {
		return direccionDestinatario;
	}
	public void setDireccionDestinatario(String direccionDestinatario) {
		this.direccionDestinatario = direccionDestinatario;
	}
	public String getUnidadNegocio() {
		return unidadNegocio;
	}
	public void setUnidadNegocio(String unidadNegocio) {
		this.unidadNegocio = unidadNegocio;
	}
	public String getEstadoOperativo() {
		return estadoOperativo;
	}
	public void setEstadoOperativo(String estadoOperativo) {
		this.estadoOperativo = estadoOperativo;
	}
	public String getCodigoUnidad() {
		return codigoUnidad;
	}
	public void setCodigoUnidad(String codigoUnidad) {
		this.codigoUnidad = codigoUnidad;
	}
	public String getTipoUnidad() {
		return tipoUnidad;
	}
	public void setTipoUnidad(String tipoUnidad) {
		this.tipoUnidad = tipoUnidad;
	}
	public String getPesoReal() {
		return pesoReal;
	}
	public void setPesoReal(String pesoReal) {
		this.pesoReal = pesoReal;
	}
	public String getLargo() {
		return largo;
	}
	public void setLargo(String largo) {
		this.largo = largo;
	}
	public String getAlto() {
		return alto;
	}
	public void setAlto(String alto) {
		this.alto = alto;
	}
	public String getAncho() {
		return ancho;
	}
	public void setAncho(String ancho) {
		this.ancho = ancho;
	}
	
	public BigDecimal getIdUnidad() {
		return idUnidad;
	}
	public void setIdUnidad(BigDecimal idUnidad) {
		this.idUnidad = idUnidad;
	}
	public String getNuevoCodigo() {
		return nuevoCodigo;
	}
	public void setNuevoCodigo(String nuevoCodigo) {
		this.nuevoCodigo = nuevoCodigo;
	}
	
	public String getUbicacionTcc() {
		return ubicacionTcc;
	}
	public void setUbicacionTcc(String ubicacionTcc) {
		this.ubicacionTcc = ubicacionTcc;
	}
	


}
