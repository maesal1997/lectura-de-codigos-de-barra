package co.com.tcc.maestro.exception;

public class AccesoDatosException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6659405421519686252L;

	public AccesoDatosException(String message) {
		super(message);
	}

	public AccesoDatosException(String message, Throwable cause) {
		super(message, cause);
	}
}