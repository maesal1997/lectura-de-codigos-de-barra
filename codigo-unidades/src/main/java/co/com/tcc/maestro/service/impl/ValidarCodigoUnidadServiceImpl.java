package co.com.tcc.maestro.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tcc.maestro.dao.IValidarCodigoUnidadDao;
import co.com.tcc.maestro.dto.ObjMensajes;
import co.com.tcc.maestro.exception.ValidacionDominioException;
import co.com.tcc.maestro.service.IValidarCodigoUnidadService;

@Service
public class ValidarCodigoUnidadServiceImpl implements IValidarCodigoUnidadService{


	@Autowired
	private IValidarCodigoUnidadDao validarCodigoUnidadDao;
	
	@Override
	public void validarCodigoUnidad(String nuevosCodigosUnidad) {

		List<String> restultadoCodigos = new ArrayList<String>();
		
		restultadoCodigos = validarCodigoUnidadDao.validarCodigoUnidad(nuevosCodigosUnidad);
		
		if (restultadoCodigos.size() > 0) {
			
			String nuevoCodigosRepetidos = "";
			for (int i = 0; i < restultadoCodigos.size(); i++) {
				nuevoCodigosRepetidos += ", " +  restultadoCodigos.get(i); 
			}
			nuevoCodigosRepetidos = nuevoCodigosRepetidos.substring(1, nuevoCodigosRepetidos.length());
			String codigosUnidad = "El o los nuevo(s) c�digo(s) de unidad ya existe(n): " + nuevoCodigosRepetidos;
			ObjMensajes objMensajes = new ObjMensajes();
			objMensajes.setMensajeSistema(codigosUnidad);
		    objMensajes.setMensajeUsuario(codigosUnidad);		
			throw new ValidacionDominioException(objMensajes);
		}
		
	}

}
