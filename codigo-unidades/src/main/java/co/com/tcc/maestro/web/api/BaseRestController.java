package co.com.tcc.maestro.web.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.tcc.maestro.dao.impl.ParametrosDao;
import co.com.tcc.maestro.utils.Parametros;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class BaseRestController {

	private static final String api = "api/base/";

	@Autowired
	private ParametrosDao parametrosDao;

	@GetMapping(value = api + "all")
	public Map<String, Object> listarTodos() {

		Map<String, Object> todo = new HashMap<String, Object>();

		todo.put(Parametros.MEDIOS_DIST_REQ_DOC, parametrosDao.consultarParametro(Parametros.MEDIOS_DIST_REQ_DOC));
		todo.put(Parametros.AUTORIZA_TRATA_DATOS, parametrosDao.consultarParametro(Parametros.AUTORIZA_TRATA_DATOS));

		return todo;
	}

	@PostMapping(value = api + "parametros")
	public Map<String, Object> consultarParametros(@RequestBody List<String> nombresParametros) {

		return parametrosDao.consultarParametros(nombresParametros);
	}

}
